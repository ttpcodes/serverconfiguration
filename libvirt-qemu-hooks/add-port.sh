#!/bin/bash
GUEST_ADDR=192.168.1.2
GUEST_NAME=node1
VIR_IFACE=virbr1
PORT_CONFIG=${PORT_CONFIG:-/etc/vm_ports.conf}

echo "${1} ${2} ${3}" >> $PORT_CONFIG
echo "Mapping ${1} ${2} on host to ${3} on guest."
virsh list | grep ${GUEST_NAME}
if [ $? == 0 ]; then
	echo "VM is running. Adding iptables rule automatically."
	/usr/sbin/iptables -I FORWARD -o ${VIR_IFACE} -d ${GUEST_ADDR} -j ACCEPT
	/usr/sbin/iptables -t nat -I PREROUTING -p ${1} --dport ${2} -j DNAT --to ${GUEST_ADDR}:${3}
fi
